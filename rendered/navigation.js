window.PatternsNavigation = {
    "children": {
        "atoms": {
            "title": "Atoms",
            "children": {
                "content": {
                    "title": "Content",
                    "children": {
                        "headings": {
                            "title": "Headings",
                            "url": "#atoms/content/headings"
                        },
                        "lists": {
                            "title": "Lists",
                            "url": "#atoms/content/lists"
                        },
                        "tables": {
                            "title": "Tables",
                            "url": "#atoms/content/tables"
                        }
                    }
                },
                "forms": {
                    "title": "Forms",
                    "children": {
                        "textfields": {
                            "title": "Textfields",
                            "url": "#atoms/forms/textfields"
                        }
                    }
                },
                "global": {
                    "title": "Global",
                    "children": {
                        "colors": {
                            "title": "Colors",
                            "url": "#atoms/global/colors"
                        },
                        "fonts": {
                            "title": "Fonts",
                            "url": "#atoms/global/fonts"
                        }
                    }
                }
            }
        },
        "molecules": {
            "title": "Molecules",
            "children": {
                "block": {
                    "title": "Block",
                    "children": {
                        "event": {
                            "title": "Event",
                            "url": "#molecules/block/event"
                        },
                        "intro": {
                            "title": "Intro",
                            "url": "#molecules/block/intro"
                        },
                        "link": {
                            "title": "Link",
                            "url": "#molecules/block/link"
                        },
                        "news": {
                            "title": "News",
                            "url": "#molecules/block/news"
                        },
                        "person": {
                            "title": "Person",
                            "url": "#molecules/block/person"
                        },
                        "social_media": {
                            "title": "Social Media",
                            "url": "#molecules/block/social_media"
                        }
                    }
                },
                "blocks": {
                    "title": "Blocks",
                    "children": {
                        "banner": {
                            "title": "Banner",
                            "url": "#molecules/blocks/banner"
                        },
                        "icon": {
                            "title": "Icon",
                            "url": "#molecules/blocks/icon"
                        },
                        "media": {
                            "title": "Media",
                            "url": "#molecules/blocks/media"
                        },
                        "search": {
                            "title": "Search",
                            "url": "#molecules/blocks/search"
                        }
                    }
                },
                "forms": {
                    "title": "Forms",
                    "children": {
                        "form": {
                            "title": "Form",
                            "url": "#molecules/forms/form"
                        }
                    }
                }
            }
        },
        "organisms": {
            "title": "Organisms",
            "children": {
                "content": {
                    "title": "Content",
                    "children": {
                        "events": {
                            "title": "Events",
                            "url": "#organisms/content/events"
                        },
                        "faq": {
                            "title": "Faq",
                            "url": "#organisms/content/faq"
                        },
                        "link_list": {
                            "title": "Link List",
                            "url": "#organisms/content/link_list"
                        },
                        "main_content": {
                            "title": "Main Content",
                            "url": "#organisms/content/main_content"
                        },
                        "news": {
                            "title": "News",
                            "url": "#organisms/content/news"
                        },
                        "people": {
                            "title": "People",
                            "url": "#organisms/content/people"
                        },
                        "photo_gallery": {
                            "title": "Photo Gallery",
                            "url": "#organisms/content/photo_gallery"
                        },
                        "social_media": {
                            "title": "Social Media",
                            "url": "#organisms/content/social_media"
                        }
                    }
                },
                "global": {
                    "title": "Global",
                    "children": {
                        "footer": {
                            "title": "Footer",
                            "url": "#organisms/global/footer"
                        },
                        "header": {
                            "title": "Header",
                            "url": "#organisms/global/header"
                        },
                        "marquee": {
                            "title": "Marquee",
                            "url": "#organisms/global/marquee"
                        },
                        "tools": {
                            "title": "Tools",
                            "url": "#organisms/global/tools"
                        }
                    }
                },
                "help": {
                    "title": "Help",
                    "children": {
                        "search": {
                            "title": "Search",
                            "url": "#organisms/help/search"
                        }
                    }
                }
            }
        },
        "templates": {
            "title": "Templates",
            "children": {
                "base": {
                    "title": "Base",
                    "children": {
                        "base": {
                            "title": "Base",
                            "url": "#templates/base/base"
                        }
                    }
                },
                "v5": {
                    "title": "V5",
                    "children": {
                        "standard": {
                            "title": "Standard",
                            "url": "#templates/v5/standard"
                        }
                    }
                }
            }
        },
        "pages": {
            "title": "Pages",
            "children": {
                "top_level": {
                    "title": "Top Level",
                    "children": {
                        "program": {
                            "title": "Program",
                            "url": "#pages/top_level/program"
                        },
                        "student_life": {
                            "title": "Student Life",
                            "url": "#pages/top_level/student_life"
                        }
                    }
                },
                "websites": {
                    "title": "Websites",
                    "children": {
                        "wellness_wall": {
                            "title": "Wellness Wall",
                            "url": "#pages/websites/wellness_wall"
                        }
                    }
                }
            }
        }
    }
};