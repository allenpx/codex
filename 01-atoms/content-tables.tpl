<div class="component content">


    <table> 
        <thead> 
            <tr> 
                <td></td> 
                <td>GIF</td> 
                <td>PNG</td> 
                <td>JPG</td> 
                <td>SWF</td> 
            </tr> 
        </thead>
        <tr> 
            <th>Compression</th> 
            <td>lossless</td> 
            <td>lossless</td> 
            <td>lossy</td> 
            <td>variable*</td> 
        </tr> 
        <tr> 
            <th>Max Colors</th> 
            <td>256 indexed</td> 
            <td>Trillions</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
        </tr> 
    </table> 


    <table> 
        <thead>
            <tr> 
                <td></td> 
                <td>GIF</td> 
                <td>PNG</td> 
                <td>JPG</td> 
                <td>SWF</td> 
                <td>SWF</td> 
                <td>SWF</td> 
                <td>SWF</td> 
                <td>SWF</td> 
                <td>SWF</td> 
            </tr> 
        </thead>
        <tr> 
            <th>Compression</th> 
            <td>lossless</td> 
            <td>lossless</td> 
            <td>lossy</td> 
            <td>variable*</td> 
            <td>variable*</td> 
            <td>variable*</td> 
            <td>variable*</td> 
            <td>variable*</td> 
            <td>variable*</td> 
        </tr> 
        <tr> 
            <th>Max Colors</th> 
            <td>256 indexed</td> 
            <td>Trillions</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
            <td>Thousands</td> 
        </tr> 
    </table> 



    <h2>Allowable Weight</h2>
    <p>Remade from <a href="http://www.uml.edu/AFROTC/programs/weight-table.aspx">here</a>.</p>
    <table>
        
        <thead>
            <th align="center">Height (in)</th>
            <th align="center">Minimum (lbs)</th>
            <th align="center">Maximum (lbs)</th>
        </thead>
        <tbody>
            <tr>
                <td align="center">58</td>
                <td align="center">91</td>
                <td data-label="Maximum (lbs)" align="center">131</td>
            </tr>
            <tr>
                <td align="center">58</td>
                <td align="center">91</td>
                <td data-label="Maximum (lbs)" align="center">131</td>
            </tr>
            <tr>
                <td align="center">58</td>
                <td align="center">91</td>
                <td data-label="Maximum (lbs)" align="center">131</td>
            </tr>
            <tr>
                <td align="center">58</td>
                <td align="center">91</td>
                <td data-label="Maximum (lbs)" align="center">131</td>
            </tr>
            <tr>
                <td align="center">58</td>
                <td align="center">91</td>
                <td data-label="Maximum (lbs)" align="center">131</td>
            </tr>
        </tbody>

    </table>




</div>






