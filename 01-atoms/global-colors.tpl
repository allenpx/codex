<ul class="pattern-library color-swatches">
    <li>
        <div class="swatch" style="background-color:#0069B1"></div>
        <div><strong>Blue</strong></div>
        <div class="info">
            <label>hex:</label><span>#0069B1</span><br/>
            <label>less variable:</label><span>@color-primary</span>
        </div>
    </li>
    <li>
        <div class="swatch" style="background-color:#67B6BD"></div>
        <div><strong>Teal</strong></div>
        <div class="info">
            <label>hex:</label><span>#67B6BD</span><br/>
            <label>less variable:</label><span>@color-secondary</span>
        </div>
    </li>
    <li>
        <div class="swatch" style="background-color:#FAC537"></div>
        <div><strong>Saffron</strong></div>
        <div class="info">
            <label>hex:</label><span>#FAC537</span><br/>
            <label>less variable:</label><span>@color-tertiary</span>
        </div>
    </li>
    <li>
        <div class="swatch" style="background-color:#F06E35"></div>
        <div><strong>Orange</strong></div>
        <div class="info">
            <label>hex:</label><span>#F06E35</span><br/>
            <label>less variable:</label><span>@color-quaternary</span>
        </div>
    </li>
</ul> 