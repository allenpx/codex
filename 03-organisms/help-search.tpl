<% data = _.extend({
    type:"website",
    maxResults:5,
    client:"",
    directory:"",
    intro:"<h2>What do you want to study?</h2><p>Lorem ipsum Dolor eu incididunt elit commodo eu labore nostrud adipisicing velit eiusmod sunt ea reprehenderit.</p>",
    placeholder:"",
    prompt:"What do you want to study?",
    searchContent:'',
    legend:null,
    more:{
        url:"#browse",
        label:"Browse All Majors and Miners"
    }
}, data); %>


<div class="component search" data-max-results="<%=data.maxResults%>" data-type="<%=data.type%>" data-client="<%=data.client%>" data-directory="<%=data.directory%>">

    
    <% if(data.intro != "" || typeof data.intro != 'undefined'){%>
    <%=data.intro%>
    <%}%>

    <div class="search-box">
        <h3><%=data.prompt%></h3>
        <div class="textfield">
            <input type="text" placeholder="<%=data.placeholder%>"/>
            <i class="icon icon-search text"></i>
            <i class="loading-icon"></i>
        </div>
        <div class="results-container">
            <div class="results"></div>
            <% if(data.legend != "" && typeof data.legend != 'undefined' && data.legend != null){%>
                <div class="legend">
                    <% if(data.legend != "example"){%>
                        <%=data.legend%>
                    <%}%>
                    <% if(data.legend === "example"){%>
                        <ul>
                            <li><strong>B/M</strong> Bachelor's to Master's Program</li>
                            <li><strong>CE</strong> Continuing Studies </li>
                            <li><strong>ON</strong> Online Program</li>
                        </ul>
                    <%}%>
                </div>
            <%}%>

        </div>
    </div>

    <% if(data.searchContent != "" || typeof data.searchContent != 'undefined'){%>
    <div class="search-content" style="display:none;">

        <% if(data.searchContent != "example"){%>
            <%=data.searchContent%>
        <%}%>

        <% if(data.searchContent === "example"){%>
            <ul>
                <li>
                    <a href=
                    "/FAHSS/American-Studies/Programs-of-Study/Minor-Page-TEST.aspx"
                    target="_self" title="American Studies">American Studies</a>
                </li>

                <li>
                    <a href="/Sciences/biology/default.aspx" target="_self" title=
                    "Biology">Biology</a> (B/M)

                    <ul>
                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Biology/Biological-Sciences-Major.aspx"
                            title="Biological Sciences Major">Bioinformatics</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Biology/Biotechnology-Option.aspx"
                            title="Biotechnology Option">Biotechnology</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Biology/Ecology-Option.aspx"
                            title="Ecology Option">Ecology</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/MSB/Undergraduate-Programs/default.aspx" target="_self"
                    title="Business Administration">Business Administration</a>

                    <ul>
                        <li>
                            <a href="/MSB/Accounting/default.aspx" title=
                            "link this for accounting landing page">Accounting</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/management/Entrepreneurship.aspx"
                            target="_self" title="Entrepreneurship">Entrepreneurship</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/management/Finance.aspx" target=
                            "_self" title="Finance">Finance</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/management/International.aspx"
                            target="_self" title="International">International Business</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/Operations-Info-Systems/default.aspx"
                            target="_self" title=
                            "Operations and Information Systems Department">Management
                            Information Systems</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/management/Management.aspx" target=
                            "_self" title="Management">Management</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/MSB/Departments/management/Marketing.aspx" target=
                            "_self" title="Marketing">Marketing</a>
                        </li>

                        <li>
                            <a href=
                            "/MSB/Departments/Operations-Info-Systems/SupplyChain-OpsMngmnt-Concentration.aspx"
                            title=
                            "Supply Chain &amp; Operations Management Concentration">Supply
                            Chain &amp; Operations Management</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Chemical/default.aspx" target="_self" title=
                    "Chemical Engineering">Chemical Engineering</a> (B/M)

                    <ul>
                        <li>
                            <a href="/Engineering/Chemical/Programs/undergrad-options.aspx"
                            target="_self" title="Biological Engineering">Biological
                            Engineering</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/Engineering/Chemical/Programs/undergrad-options.aspx"
                            target="_self" title="Nanomaterials Engineering">Nanomaterials
                            Engineering</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/Engineering/Chemical/Programs/undergrad-options.aspx"
                            target="_self" title="Nuclear Engineering">Nuclear
                            Engineering</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="/Engineering/Chemical/Programs/undergrad-options.aspx"
                            target="_self" title="Paper Engineering">Paper Engineering</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href=
                    "/Sciences/chemistry/Programs-of-Study/Undergraduate/Undergraduate.aspx"
                    target="_self" title="Chemistry">Chemistry</a>

                    <ul>
                        <li>
                            <a href=
                            "/Sciences/Bio-Cheminformatics/Programs/Undergraduate.aspx"
                            target="_self" title="Cheminformatics">Cheminformatics</a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Chemistry/Forensic-Science-Option.aspx"
                            title="Forensic Science Option">Forensic Science</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Civil-Environmental/default.aspx" target="_self"
                    title="Civil Engineering">Civil Engineering</a> (B/M)
                </li>

                <li>
                    <a href="/Health-Sciences/CLNS/default.aspx" target="_self" title=
                    "Clinical Lab Sciences">Clinical Lab Sciences</a> (B/M)
                </li>

                <li>
                    <ul>
                        <li>
                            <a href="http://www.uml.edu/SHE/CLNS/cls_med_tec_opt.html"
                            target="_self"></a><a href=
                            "/Health-Sciences/CLNS/Programs/med-lab-sci.aspx" title=
                            "Medical Laboratory Science">Medical Laboratory
                            Science</a>&nbsp;(formerly Medical Technology)
                        </li>

                        <li>
                            <a href=
                            "/Health-Sciences/CLNS/Programs/cls-clinicalsci-option.aspx"
                            title=
                            "Clinical Lab Sciences, Clinical Sciences Option">Clinical
                            Science</a>
                        </li>
                    </ul>
                </li>

                <li>Community Health&nbsp;

                    <ul>
                        <li>Environmental Health</li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Electrical-Computer/default.aspx" target="_self"
                    title="Computer Engineering">Computer Engineering</a> (B/M)
                </li>

                <li>
                    <a href="/Sciences/computer-science/Programs/Ugrad/default.aspx"
                    target="_self" title="Computer Science">Computer Science</a> (B/M)

                    <ul>
                        <li>
                            <a href="http://www.uml.edu/sciences/bio-cheminformatics/"
                            target="_self" title=
                            "Bioinformatics/Cheminformatics">Bioinformatics/Cheminformatics</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/FAHSS/Criminal-Justice/default.aspx" title=
                    "link this for Criminal-Justice landing page">Criminology &amp; Justice
                    Studies</a>

                    <ul>
                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Corrections</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Homeland Security</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Information and Technology</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Police</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Research &amp; Evaluation</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/Criminal-Justice/Programs-of-Study/undergrad-programs.aspx"
                            title="Undergrad-Programs">Violence</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/FAHSS/economics/Programs-of-Study.aspx" target="_self" title=
                    "Economics">Economics</a>
                </li>

                <li>
                    <a href="/Education/Undergraduate-minors/Minor.aspx" target="_self"
                    title="Undergrad Education Minor">Education</a>&nbsp;(minor)
                </li>

                <li>
                    <a href="/Engineering/Electrical-Computer/default.aspx" target="_self"
                    title="Electrical Engineering">Electrical Engineering</a>&nbsp;(B/M)
                </li>

                <li>
                    <a href="/FAHSS/English/Programs-of-Study/Programs-of-Study.aspx"
                    target="_self" title="English">English</a>
                </li>

                <li>
                    <ul>
                        <li>
                            <a href=
                            "/FAHSS/English/Programs-of-Study/Concentration-Creative.aspx"
                            target="_self" title="Creative Writing">Creative Writing</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/English/Programs-of-Study/Concentration-Journalism.aspx"
                            target="_self" title=
                            "Journalism &amp; Professional Writing">Journalism &amp;
                            Professional Writing</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/English/Programs-of-Study/Concentration-Literature.aspx"
                            target="_self" title="Literature">Literature</a>
                        </li>

                        <li>
                            <a href="http://www.uml.edu/fahss/theater-Arts/default.html"
                            target="_self" title="Theater Arts">Theater Arts</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Sciences/EEAS/Program-of-Study/undergrad.aspx" target="_self"
                    title="Environmental, Earth &amp; Atmospheric Science">Environmental,
                    Earth, &amp; Atmospheric Science</a>

                    <ul>
                        <li>
                            <a href="/Sciences/EEAS/Program-of-Study/atmospheric.aspx"
                            title="Atmospheric Science - Meteorology">Atmospheric
                            Science&nbsp;- Meteorology</a>
                        </li>

                        <li>
                            <a href="/Sciences/EEAS/Program-of-Study/environmental.aspx"
                            target="_self" title="Environmental Studies">Environmental
                            Studies</a>
                        </li>

                        <li>
                            <a href="/Sciences/EEAS/Program-of-Study/geoscience.aspx"
                            target="_self" title="Geoscience - Geology">Geoscience&nbsp;-
                            Geology</a>
                        </li>
                    </ul>
                </li>

                <li>Environmental Health</li>

                <li>
                    <a href="/Health-Sciences/PT/Exercise-Physiology/default.aspx" title=
                    "Exercise Physiology - Physical Therapy">Exercise Physiology&nbsp;-
                    Physical Therapy&nbsp;</a>
                </li>

                <li>
                    <a href="http://www.uml.edu/dept/art/index.htm" target="_self" title=
                    "Fine Arts">Fine Arts</a>

                    <ul>
                        <li>
                            <a href="http://www.uml.edu/Dept/Art/areas-of-study.htm" title=
                            "Art">Art</a>
                        </li>

                        <li>
                            <a href="http://www.uml.edu/Dept/Art/areas-of-study.htm" title=
                            "Graphic Design">Graphic Design</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/FAHSS/History/Programs-of-Study.aspx" title=
                    "History Programs of Study">History</a>

                    <ul>
                        <li>Art History</li>
                    </ul>
                </li>

                <li>
                    <a href="/FAHSS/BLA/default.aspx" title="Liberal Arts">Liberal
                    Arts&nbsp;</a>
                </li>

                <li>
                    <a href="/Sciences/mathematics/programs/Programs-of-Study.aspx" target=
                    "_self" title="Mathematics">Mathematics</a> (B/M)
                </li>

                <li>
                    <ul>
                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-applied-computational-math-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-applied-computational-math-option">Applied/Computational
                            Mathematics<br></a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-bioinformatics-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-bioinformatics-option">Bioinformatics</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-business-applications-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-business-applications-option">Business
                            Applications</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-computer-science-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-computer-science-option">Computer Science</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-probability-statistics-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-probability-statistics">Probability/Statistics</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Mathematical-Sciences/Degree-Pathways/DP-math-teaching-option.aspx"
                            target="_blank" title=
                            "DP-sciences-math-teaching-option">Teaching</a>&nbsp;(B/M)
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Mechanical/default.aspx" target="_self" title=
                    "Mechanical Engineering">Mechanical Engineering</a> (B/M)
                </li>

                <li>
                    <a href=
                    "/FAHSS/Languages-Cultures/Programs-of-Study/Bachelor-of-Arts-in-Modern-Languages.aspx"
                    target="_self" title="Bachelor of Arts in Modern Languages">Modern
                    Languages</a>

                    <ul>
                        <li>Spanish</li>

                        <li>
                            <a href=
                            "/docs/Modern%20Languages%20-%20Spanish%20%20French%20Option%2014_tcm18-651.pdf"
                            target="_blank" title="Spanish/French">Spanish/French</a>
                        </li>

                        <li>
                            <a href=
                            "/docs/Modern%20Languages%20-%20Spanish%20%20Italian%20Option%2014_tcm18-652.pdf"
                            target="_blank" title="Spanish-Italian">Spanish/Italian</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href=
                    "/FAHSS/music/Programs/Undergraduate/Music-Business/Music-Business-Program.aspx"
                    title="Music Business">Music Business</a>
                </li>

                <li>
                    <a href=
                    "/FAHSS/music/Programs/Undergraduate/Music-Performance/Performance-Program.aspx"
                    title="Music Performance">Music Performance</a>

                    <ul>
                        <li>
                            <a href=
                            "/FAHSS/music/Programs/Undergraduate/Music-Performance/Performance-Program.aspx"
                            title="Instrumental">Instrumental</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/music/Programs/Undergraduate/Music-Performance/Performance-Program.aspx"
                            title="Voice">Voice</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href=
                    "/FAHSS/music/Programs/Undergraduate/Music-Studies/Music-Studies.aspx"
                    title="Music Studies">Music Studies</a> (B/M)

                    <ul>
                        <li>
                            <a href=
                            "/FAHSS/music/Programs/Undergraduate/Music-Studies/Music-Studies.aspx"
                            title="Instrumental">Instrumental</a>
                        </li>

                        <li>
                            <a href=
                            "/FAHSS/music/Programs/Undergraduate/Music-Studies/Music-Studies.aspx"
                            title="Voice">Voice</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Chemical/Programs/undergraduate-programs.aspx"
                    target="_self" title="Nuclear Engineering">Nuclear Engineering</a>
                </li>

                <li>
                    <a href="/Health-Sciences/Nursing/Programs/Bachelors/bac-pro.aspx"
                    target="_self" title="Nursing">Nursing</a>
                </li>

                <li>
                    <a href="/Health-Sciences/CLNS/Programs/nutritional-sciences.aspx"
                    target="_self" title="Nutritional Science Major">Nutritional
                    Science</a>
                </li>

                <li>
                    <a href=
                    "/FAHSS/Peace-and-Conflict-Studies/Programs/Undergraduate-Program.aspx"
                    target="_self" title="Peace &amp; Conflict Studies">Peace &amp;
                    Conflict Studies</a>&nbsp;(B/M)
                </li>

                <li>
                    <a href="/FAHSS/Philosophy/Programs-of-Study/Philosophy-Major.aspx"
                    target="_self" title="Philosophy Major">Philosophy</a>

                    <div style="margin-left: 2em">
                        <ul>
                            <li>
                                <a href=
                                "/FAHSS/Philosophy/Programs-of-Study/Communications-and-Critical-Thinking.aspx"
                                target="_self" title=
                                "Communications and Critical Thinking">Communications and
                                Critical Thinking</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="/Sciences/physics/Programs-of-Study/default.aspx" target=
                    "_self" title="Physics">Physics</a> (B/M)

                    <ul>
                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Physics/Degree-Pathways/DP-physics-photonics.aspx"
                            target="_blank" title=
                            "DP-sciences-physics-photonics">Photonics</a>
                        </li>

                        <li>
                            <a href=
                            "/Catalog/Undergraduate/Sciences/Physics/Degree-Pathways/DP-Physics-radiological-health.aspx"
                            target="_blank" title=
                            "DP-sciences-Physics-radiological-health">Radiological
                            Health</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/Engineering/Plastics/default.aspx" target="_self" title=
                    "Plastics Engineering">Plastics Engineering</a> (B/M)
                </li>

                <li>
                    <a href="/FAHSS/Political-Science/default.aspx" target="_self" title=
                    "Political Science ">Political Science</a>
                </li>

                <li>
                    <a href="/FAHSS/Psychology/Undergraduate-Program/default.aspx" target=
                    "_self" title="Psychology">Psychology</a> (B/M)&nbsp;

                    <ul>
                        <li>
                            <a href=
                            "/FAHSS/Psychology/Undergraduate-Program/Developmental-Disabilities.aspx"
                            target="_self" title=
                            "Specialization in Development Disabilities">Developmental
                            Disabilities</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href=
                    "/Health-Sciences/Public-Health/Programs-of-Study/Undergraduate.aspx"
                    title="Undergraduate">Public Health</a>
                </li>

                <li>
                    <a href="/FAHSS/Sociology/Programs-of-Study/major.aspx" target="_self"
                    title="Sociology Department at UMass Lowell">Sociology</a>
                </li>

                <li>
                    <a href="/FAHSS/music/SRT/Sound-Recording-Technology.aspx" title=
                    "Sound Recording Technology">Sound Recording Technology</a>
                </li>
            </ul>
            
        <%}%>
    </div>
    <%}%>

    <%if(typeof data.more != "undefined" && data.more != null){ %>
        <div class="more">
            <a href="<%=data.more.url%>"><%if(typeof data.more.icon != "undefined"){ %><i class="<%=data.more.icon%>"></i><%}%><%=data.more.label%></a>
        </div>
    <%}%>

</div>