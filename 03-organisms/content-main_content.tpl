<% data = _.extend({
    classes:''
}, data); 

if(typeof data.sections == 'undefined'){
    data.sections = [_.clone(data)];
}

%>

<% if(typeof data.intro != 'undefined' && data.intro != '' && data.intro != null){%>
<%= _.partial('molecules-block-intro', {
    text:data.intro
}) %>
<% }%> 

<div class="component content <%= data.classes %>">
    


    <% if(data.classes.indexOf('collage') != -1){ %>

        <div class="inside">

            <%if(typeof data.heading != 'undefined'){%>
                <h2><span><%= data.heading %></span></h2>
            <%}%>

            <% _.each(data.sections, function(section){ %>

                <% if(typeof section.media != 'undefined'){%>
                    <% if(typeof section.media.style == 'undefined'){section.media.style = ''}%>
                    <figure class="block <%= section.media.style %>">
                        <% if(section.media.type == 'video'){%>
                        <div class="video"><iframe src="<%=section.media.url%>" allowfullscreen></iframe></div>
                        <% }%>
                        <% if(section.media.type == 'image'){%>
                        <div class="image"><img src="<%=section.media.url%>"/></div>
                        <% }%> 
                    </figure>
                <% }%>

                <% if(_.isArray(section.text)){
                    section.text = section.text.join('');
                }%>
                <div class="block text">
                <% if(typeof section.heading != 'undefined'){%>
                    <h3><%= section.heading %></h3>
                <% }%>
                <%= section.text %>
                </div>

            <% }) %>

        </div>

        <%if(data.classes.indexOf('resources') != -1){%>
        <div class="additional">
            <%if(data.classes.indexOf('resources') != -1){%>
                <h2>Most Visited Links</h2>
            <%}%>   
        </div>
        <%}%>

    <% }else{ %> 

         <div class="inside">
            <%if(typeof data.heading != 'undefined'){%>
                <h2><%= data.heading %></h2>
            <%}%>

            <div class="sections"> 
            <% _.each(data.sections, function(section){ %>
                <div class="section">

                    <% if(typeof section.media != 'undefined'){%>
                        <% if(typeof section.media.style == 'undefined'){section.media.style = ''}%>
                        <figure class="<%= section.media.style %>">
                            <% if(section.media.type == 'video'){%>
                            <div class="video"><iframe src="<%=section.media.url%>" allowfullscreen></iframe></div>
                            <% }%>
                            <% if(section.media.type == 'image'){%>
                            <div class="image"><img src="<%=section.media.url%>"/></div>
                            <% }%> 
                        </figure>
                    <% }%>

                    <% if(_.isArray(section.text)){
                        section.text = section.text.join('');
                    }%>
                    <div class="text">
                    <% if(typeof section.heading != 'undefined'){%>
                        <h3><%= section.heading %></h3>
                    <% }%>
                    <%= section.text %>
                    </div>

                    
                </div>
            <% }) %>
            </div>
        </div>

        <%if(data.classes.indexOf('resources') != -1){%>
        <div class="additional">
            <%if(data.classes.indexOf('resources') != -1){%>
                <h2>Most Visited Links</h2>
            <%}%>   
        </div>
        <%}%>


    <% }%> 

 </div>














