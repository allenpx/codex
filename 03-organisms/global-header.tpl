<% data = _.extend({
    classes:''
}, data); %>

<header class="primary <%=data.classes%>">
    
    <a href="#" class="logo"><img src="/system/template/v5/images/logo.svg"></a>

    <nav class="tools">
        <ul>
            <li><a href="#">Contact</a></li>
            <li><a href="#">iSiS</a></li>
            <li><a href="#">Email</a></li>
            <li><a href="#">Blackboard</a></li> 
            <li><a href="#makeagift">Make a Gift</a></li> 
            <li><a href="/search"><i class="icon icon-search white flip-horizontal"></i></a></li>
        </ul>
    </nav>

    <nav class="primary">
        <ul>
<!--             <li><a href="#">Lowell Experience</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Academics</a></li>
            <li><a href="#">Research</a></li>
            <li><a href="#">Admissions &amp; Aid</a></li>
            <li><a href="#">Student Life</a></li>
            <li><a href="#">Athletics</a></li> -->

<li><a href="/About/default.aspx">About</a></li><li><a href="/Academics/default.aspx">Academics</a></li><li><a href="/Research/default.aspx">Research</a></li><li><a href="/Admissions-Aid/default.aspx">Admissions &amp; Aid</a></li><li><a href="/Today/Students/default.aspx">Student Life</a></li><li><a href="/Athletics-Recreation/default.aspx">Athletics &amp; Recreation</a></li>

        </ul>
    </nav>


</header>