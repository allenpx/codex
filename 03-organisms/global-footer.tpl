<footer>
	<div class="col a"></div>
	<div class="col b">
		<h2>Menu</h2>
		<ul>
			<li><a href="/About/default.aspx">About</a></li>
			<li><a href="/Academics/default.aspx">Academics</a></li>
			<li><a href="/Research/default.aspx">Research</a></li>
			<li><a href="/Admissions-Aid/default.aspx">Admissions &amp; AID</a></li>
			<li><a href="/Today/Students/default.aspx">Student Life</a></li>
			<li><a href="/Athletics-Recreation/default.aspx">Athletics &amp; Recreation</a></li>
		</ul>
	</div>
	<div class="col c">
		<a href="/" class="logo"><img src="/system/template/v5/images/logo-footer.svg"></a>
		<address>
    		<p>One University Avenue . Lowell, MA 01854 . 978-934-4000 - <a href="/Directory/Question.aspx" title="question">Contact Us</a> <strong>Undergraduate Admissions</strong> - University Crossing, Suite 420, 220 Pawtucket St., Lowell, MA 01854-2874</p>
			<p><a href="http://www.umassonline.net" target="_blank">UMassOnline</a> | <a href="http://www.umassclub.com" target="_blank">UMass Club</a> | <a href="http://www.massachusetts.edu" target="_blank">UMass System</a></p>                    
		</address>
	</div>
	<div class="float-clear"></div>
</footer>
