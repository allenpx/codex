<% data = _.extend({
    banner:"http://fpoimg.com/1200x700?text=Banner",
    title:"Lorem Ipsum Nisi enim Reprehenderit",
}, data); %>


<div class="marquee <% if(data.banner != ""){%>has-banner<%}%>">
    <% if(data.banner != ""){%>
    <div class="image" style="background-image:url('<%=data.banner%>')"><img src="<%=data.banner%>"/></div>
    <%}%>
    <div class="page-title-container-outer"><div class="page-title-container"><div class="page-title"><h1><%=data.title%></h1></div></div></div>
</div>