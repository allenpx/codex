<% data = _.extend({
    title:"",
    classes:"type-a",
    twitterHandle:"UMassLowell",
    facebookId:"11833797693",
    instagramId:"212210715",
    youtubeChannelId:"UCnoWr6aW1pgogWhXlNsOaxQ",
    countpersite:6,
}, data); %>

<div class="component social-media <%=data.classes%>"
    data-twitter-handle="<%=data.twitterHandle%>"
    data-facebook-id="<%=data.facebookId%>"
    data-instagram-id="<%=data.instagramId%>"
    data-youtubeChannel-id="<%=data.youtubeChannelId%>"
    data-counter-per-site="<%=data.countpersite%>"
>

    <%if(typeof data.title != "undefined" && typeof data.title != ""){ %>
    <h2><%=data.title%></h2>
    <%}%>

    <div class="area">

    </div>

</div>