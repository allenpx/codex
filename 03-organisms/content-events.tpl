<% data = _.extend({
    title:"Events",
    classes:'horizontal',
    items:[
        {
            date:"2015-10-11 09:00",
            title:"Student Community Engagement & Research Symposium",
            url:"http://www.uml.edu/calendar/#/event=10497506",
            id:"10497506",
        },
        {
            date:"2015-10-11 09:00",
            title:"UMass Lowell Sustainability Festival",
            id:"10501272",
            url:"http://www.uml.edu/calendar/#/event=10501272",
        },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    ],
    more:{
        url:"#",
        icon:"icon icon-calendar primary",
        label:"More Events"
    }
}, data); %>

<div class="component events <%=data.classes%>">

    <%if(typeof data.title != "undefined" && typeof data.title != ""){ %>
    <h2><%=data.title%></h2>
    <%}%>
    <div class="ul-wrapper">
    <ul>
        <% _.each(data.items, function(item){ %>
            <%= _.partial('molecules-block-event', item) %>
        <%})%>
    </ul>
    </div>

    <%if(typeof data.more != "undefined"){ %>
        <div class="more">
            <a href="<%=data.more.url%>"><%if(typeof data.more.icon != "undefined"){ %><i class="<%=data.more.icon%>"></i><%}%><%=data.more.label%></a>
        </div>
    <%}%>


</div>