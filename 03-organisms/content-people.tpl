<% data = _.extend({
    heading:"",
    classes:"",
    items:[
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    ]
}, data); %>

<div class="component people <%=data.classes%>">
 
    <%if(data.heading != ""){ %>
    <h2><%=data.heading%></h2>
    <%}%>
    <div class="ul-wrapper">
        <ul>
        <% _.each(data.items, function(item){ %>
            <li><%= _.partial('molecules-block-person', item) %></li>
        <%})%>
        </ul>
    </div>

    <%if(typeof data.more != "undefined"){ %>
        <div class="more">
            <a href="<%=data.more.url%>"><%if(typeof data.more.icon != "undefined"){ %><i class="<%=data.more.icon%>"></i><%}%><%=data.more.label%></a>
        </div>
    <%}%>
</div>  