<% data = _.extend({
	title:"News",
	items:[
		{},
		{},
		{},
		{},
	]
}, data); %>

<div class="component news-list">

	<%if(typeof data.title != "undefined" && typeof data.title != ""){ %>
	<h2><%=data.title%></h2>
	<%}%>

	<ul>
		<% _.each(data.items, function(item){ %>
			<%= _.partial('molecules-block-news', item) %>
		<%})%>
	</ul>

    <%if(typeof data.more != "undefined"){ %>
        <div class="more">
            <a href="<%=data.more.url%>"><%if(typeof data.more.icon != "undefined"){ %><i class="<%=data.more.icon%>"></i><%}%><%=data.more.label%></a>
        </div>
    <%}%>

</div>