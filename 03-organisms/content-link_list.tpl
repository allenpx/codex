<% data = _.extend({
    title:"",
    classes:"",
    items:[
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    ]
}, data); %>

<div class="component link-list <%=data.classes%>">
 
    <%if(data.title != ""){ %>
    <h2><%=data.title%></h2>
    <%}%>
    <ul>
        <% _.each(data.items, function(item){ %>
            <li><%= _.partial('molecules-block-link', item) %></li>
        <%})%>
    </ul>
</div>  