<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10 no-flexbox" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <title></title>
    
    <meta name="viewport" content="minimal-ui, width=device-width,initial-scale=1, user-scalable=no">

    <link href="/system/template/v5/css/template.base.dev.css" rel="stylesheet" type="text/css" />
    <link href="/system/template/v5/css/template.responsive.dev.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/804448/791924/css/fonts.css" />

    <script type="text/javascript">window.afterLoad = [];</script>

</head>

<body class=""><form name="form" runat="server" id="form" encType="multipart/form-data">


    <%= _.partial('organisms-global-header', {
        //classes:'dark-primary-nav'
    }) %>

    <%= _.partial('organisms-global-marquee', {
        //banner:null,
        banner:"/system/template/v5/images/photos/homecoming.jpg"
    }) %>
 



    <div class="layout page-container" style="">

        <div class="sidebar">

            <nav class="secondary">

                <h2><span><a href="#">Website Title</a></span></h2>
                <ul>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Sit amet consectetuer</a></li>
                    <li><a href="#">Elit sed</a></li>
                    <li><a href="#">Diam nonummy nibh</a></li>
                    <li><a href="#">Tincidunt ut</a></li>
                    <li><a href="#">Laoreet dolore magna</a></li>
                    <li><a href="#">Erat volutpat</a></li>
                </ul>
            </nav>

        </div>
        <div class="page">
            
            <div class="components">

                <%= _.partial('organisms-help-search', {
                    type:"content",
                    searchContent:'example',
                    placeholder:"Ex: Business, Engineering, Homeland Security, Health",
                    legend:'example'
                }) %>

                <%= _.partial('organisms-help-search', {
                    type:"website",
                    intro:'',
                    client:"solutioncenter_frontend",
                    prompt:"What do you need help with?",
                    placeholder:"Ex: Register for classes, pay my bill, transfer credits",
                    more:null
                }) %> 

                <%= _.partial('organisms-help-search', {
                    type:"website",
                    intro:'',
                    directory:"/falcons",
                    prompt:"Wanna see some birds?",
                    more:null
                }) %> 


                <%= _.partial('organisms-content-main_content', {
                    "heading":"Resources",
                    "classes":"resources",
                    "sections":[
                        {
                            "heading":"Lorem ipsum Voluptate",
                            "text":[
                                "<ul>",
                                    "<li><a href='#'>Dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Laborum dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Nostrud dolor ullamco</a></li>",
                                    "<li><a href='#'>Dolor nostrud</a></li>",
                                    "<li><a href='#'>Ullamco nostrud</a></li>",
                                "</ul>"
                            ]
                        },
                        {
                            "heading":"Lorem ipsum Voluptate",
                            "text":[
                                "<ul>",
                                    "<li><a href='#'>Dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Laborum dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Nostrud dolor ullamco</a></li>",
                                    "<li><a href='#'>Dolor nostrud</a></li>",
                                    "<li><a href='#'>Ullamco nostrud</a></li>",
                                "</ul>"
                            ]
                        },
                        {
                            "heading":"Lorem ipsum Voluptate",
                            "text":[
                                "<ul>",
                                    "<li><a href='#'>Dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Laborum dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Nostrud dolor ullamco</a></li>",
                                    "<li><a href='#'>Dolor nostrud</a></li>",
                                    "<li><a href='#'>Ullamco nostrud</a></li>",
                                "</ul>"
                            ]
                        },
                        {
                            "heading":"Lorem ipsum Voluptate",
                            "text":[
                                "<ul>",
                                    "<li><a href='#'>Dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Laborum dolor ullamco nostrud</a></li>",
                                    "<li><a href='#'>Nostrud dolor ullamco</a></li>",
                                    "<li><a href='#'>Dolor nostrud</a></li>",
                                    "<li><a href='#'>Ullamco nostrud</a></li>",
                                "</ul>"
                            ]
                        }
                    ]
                }) %>



                <%= _.partial('organisms-content-news', {
                    title:""
                }) %>

                <%= _.partial('organisms-content-events', {
                    title:"Events"
                }) %>




                <div class="component content  appeared">
    

        
        <div class="section">
            
                  

            

            
            <p>UMass Lowell research centers differ from one another in focus and scope, but each contribute in unique ways to the common goals of expanding knowledge, generating new discoveries, and having a positive impact on society through informing policy and systemic change. Interdisciplinary collaborations are promoted by the research centers both within the university and among institutions external to UMass Lowell.&nbsp;</p>
<ul class="grid grid-medium-2 grid-xlarge-3">
<li><a href="/Rising/Centers-Institutes/Advanced-Materials.aspx" title="Center for Advanced Materials">Center for Advanced Materials</a></li>
<li><a href="/Rising/Centers-Institutes/CPHHD.aspx" title="Center for Population Health &amp; Health Disparities">Center for Population Health &amp; Health Disparities</a></li>
<li><a href="/Rising/Centers-Institutes/CPH-NEW.aspx" title="Center for the Promotion of Health in the New England Workplace">Center for the Promotion of Health in the New England Workplace</a></li>
<li><a href="/Rising/Centers-Institutes/CTSS.aspx" title="Center for Terrorism &amp; Security Studies">Center for Terrorism &amp; Security Studies</a></li>
<li><a href="/Rising/Centers-Institutes/Wind-Energy.aspx" title="Wind Energy Research Group">Center for Wind Energy</a></li>
<li><a href="/Rising/Centers-Institutes/CWW.aspx" title="Center for Women &amp; Work">Center for Women &amp; Work</a></li>
<li><a href="/Rising/Centers-Institutes/Nanomanufacturing-Center.aspx" title="Nanomanufacturing Center">Nanomanufacturing Center</a></li>
<li><a href="/Rising/Centers-Institutes/NERVE.aspx" title="New England Robotics Validation &amp; Experimentation Center">New England Robotics Validation &amp; Experimentation Center</a></li>
<li><a href="/Rising/Centers-Institutes/RURI.aspx" title="Raytheon-UMass Lowell Research Institute">Raytheon-UMass Lowell Research Institute</a></li>
<li><a href="/Rising/Centers-Institutes/Innovation-Institute.aspx" title="UMass-Innovation-Institute">UMass Innovation Institute</a></li>
</ul>    
             

        </div>
                                


</div>


                <div class="component content">
                    <p>Lorem ipsum In ullamco sint velit reprehenderit veniam ad enim exercitation minim sunt eu aliquip sint culpa et aliquip sunt do amet et sunt ut adipisicing id elit in ut sunt commodo ut laborum laborum aliqua pariatur enim incididunt aute nisi sunt nostrud et minim dolore reprehenderit et Duis labore deserunt aliqua ad sint ut veniam deserunt sit eu sunt reprehenderit minim reprehenderit magna ex.</p>

                    <p>Cillum minim id ea et labore mollit enim enim irure laborum incididunt do ad ea enim sunt aliquip adipisicing fugiat elit in nostrud veniam eu voluptate minim ullamco consectetur ad amet do consectetur officia adipisicing eiusmod Excepteur laboris esse aute irure consectetur fugiat officia ea aute Ut nisi eu ex irure ad velit quis laboris magna velit anim Duis mollit aliqua in in eiusmod do magna nulla ullamco sit veniam proident dolore eiusmod elit exercitation anim ut anim dolore non do deserunt commodo quis Duis est quis id reprehenderit amet anim do in exercitation anim eiusmod Duis dolore esse nostrud dolore.</p> 
                    
                    <p>Veniam adipisicing irure dolore voluptate aute dolor ad nisi eiusmod pariatur in nulla quis commodo dolore dolore culpa incididunt ullamco pariatur laborum dolor nisi dolor eu voluptate ex ea ea officia amet ad dolore est irure laborum cupidatat quis irure irure Excepteur sed ex ex in dolor in in dolore dolor mollit dolor nostrud velit tempor cupidatat laboris tempor in anim consectetur mollit ex incididunt dolor magna commodo laboris consequat dolor pariatur qui non.</p>
                </div>



                <%= _.partial('organisms-content-people', {
                    heading:"People (detail)",
                    classes:'detail',
                    items:[
                        {},
                        {},
                    ]
                }) %>

                <%= _.partial('organisms-content-people', {
                    heading:"People (grid-large)",
                    classes:'grid-large',
                    more:{
                        url:"#",
                        label:"More"
                    },
                    items:[
                        {},
                        {},
                        {},
                        {},
                    ]
                }) %>

                <%= _.partial('organisms-content-people', {
                    heading:"People (grid-small)",
                    classes:'grid-small',
                    items:[
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                        {},
                    ]
                }) %>


                <%= _.partial('organisms-content-main_content', {
                    "heading":"Lorem Ipsum Voluptate",
                    "classes":"three-columns", 
                    "sections":[
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },

                    ]
                }) %>
 

                <%= _.partial('organisms-content-main_content', {
                    "heading":"Lorem Ipsum Voluptate",
                    "classes":"four-columns", 
                    "sections":[
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/photo-1.jpg",
                                "style":"center"
                            },
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },

                    ]
                }) %>


                <%= _.partial('atoms-content-tables', {}) %>

            </div>

            <div class="margin-clear"></div>
        </div>
    </div>
    <div class="float-clear"></div>

    <%= _.partial('organisms-global-footer', {}) %>

    <script src="/system/template/v5/js/template.dev.js" type="text/javascript"></script>

    <!-- ### Remove in Production 
    ###-->
    <script type="text/javascript" src="http://129.63.83.158:6542/livereload.js"></script>
    <!-- ### Remove in Production ###-->


</form></body>

</html>











