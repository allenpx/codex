<% data = _.extend({
    text:""
}, data); 
%>


<div class="component intro <%= data.classes %>">

    <% if(_.isArray(data.text)){
        data.text = data.text.join('');
    }%>
    <%= data.text %>

 </div>