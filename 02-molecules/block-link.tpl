<% data = _.extend({
    image:"http://fpoimg.com/400x400?text=",
    title:"Lorem Ipsum Nisi enim Reprehenderit",
    url:"#"
}, data); %>
 
<a href="<%=data.url%>" class="block-link">
    <% if(data.image != ""){ %><div class="image">
        <img src="<%=data.image%>"/>
    </div><% } %>
    <div class="text"><span><%=data.title%></span></div>
</a>