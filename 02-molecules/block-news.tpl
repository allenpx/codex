<% data = _.extend({
	image:"http://fpoimg.com/400x400?text=",
	title:"Lorem Ipsum Nisi enim Reprehenderit",
	url:"#"
}, data); %>

<li class="block-news">

	<div class="media">
		<a class="image" href="<%=data.url%>" style="background-image:url(<%=data.image%>);">
			<img src="<%=data.image%>">
			<div class="title"><%=data.title%></div>
		</a>
	</div>

	<div class="content">
		<a class="title" href="<%=data.url%>"><%=data.title%></a>
		<%if(typeof data.description != "undefined"){ %>
		<div class="description">
			<%=data.description%>
			<%if(typeof data.date != "undefined"){ %>
			<time pubdate=""><%=data.date%></time>
			<%}%>
		</div>
		<%}%>
	</div>
</li>

