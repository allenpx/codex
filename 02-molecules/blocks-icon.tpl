[[ if (typeof classes == "undefined"){ classes = ''; } ]]
[[if(typeof href == "undefined"){]]
	<div class="icon {{classes}}">
[[}else{]]
	<a href="{{href}}" class="icon {{classes}}">
[[}]]

	[[if(typeof icon == "undefined"){]]
		<i class="uml uml-piggybank white back-orange padded rounded xlarge"></i>
	[[}else{]]
		<i class="{{icon}}"></i>
	[[}]]

	[[if(typeof text == "undefined"){]]
		<div class="text">Lorem ipsum Id deserunt ea deserunt voluptate.</div>
	[[}else{]]
		<div class="text">{{text}}</div>
	[[}]]

[[if(typeof href == "undefined"){]]
	</div>
[[}else{]]
	</a>
[[}]]
