<div class="search">
	<h3>What do you want to study?</h3>
	<div class="textfield icon-right">
		<i class="uml uml-search"></i>
		<input type="text" placeholder="Ex: Business, Engineering, Homeland Security, Health"/>
	</div>
</div> 