[[ if (typeof classes == "undefined"){ classes = ''; } ]]
<div class="banner {{classes}}">
    [[if(typeof image == "undefined"){]]
        <div class="image" style="background-image:url(http://fpoimg.com/1200x600?text=)">
            <img src="http://fpoimg.com/1200x600?text="/>
        </div>
    [[}else{]]
        <div class="image" style="background-image:url({{image}})">
            <img src="{{image}}"/>
        </div>
    [[}]]

    <div class="row">
        <div class="column small-12 medium-10 large-12 xlarge-6 xlarge-push-1">
                <div class="text">
                    [[if(typeof title == "undefined"){]]
                        <h2>Lorem Ipsum Dolore</h2>
                    [[}else{]]
                        <h2>{{title}}</h2>
                    [[}]]
                    <p> 
                        [[if(typeof text == "undefined"){]]
                            Lorem ipsum Dolore sint in et aliquip dolore culpa in do veniam in ut magna Excepteur culpa dolor tempor et.
                        [[}else{]]
                            {{text}}
                        [[}]]

                        [[if(typeof more == "undefined"){]]
                            <a href="#">Read More</a>
                        [[}else{]]
                            <a href="#">{{more}}</a>
                        [[}]]
                        
                    </p>
                </div>
        </div>
    </div>
</div> 