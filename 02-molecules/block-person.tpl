<% data = _.extend({
    image:"http://fpoimg.com/400x400?text=",
    title:"Lorem Ipsum Nisi enim Reprehenderit",
    name:"Meg Bond",
    accessory:"Neurobiology & neurodegeneration",
    description:"<h4>Director, Center for Women &amp; Work</h4> <p>Meg Bond is co-principal investigator for the NSF Advancing STEM Women in the Academy grant.</p>",
    url:"#",
}, data); %>




<div class="block-person">
    <div class="media">
        <a class="image" href="<%=data.url%>">
            <img src="http://www.uml.edu/Images/bond-opt_tcm18-167579.jpg">
            <%if(typeof data.accessory != "undefined" && data.accessory != ""){ %>
            <div class="accessory">Neurobiology &amp; neurodegeneration</div>
            <%}%>
        </a>
    </div>
    <div class="content">
        <h3><a href="<%=data.url%>"><%=data.name%></a></h3>
        <%=data.description%>
    </div>
</div>