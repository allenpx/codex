<% data = _.extend({
    date:"2015-10-11 09:00",
    title:"Lorem Ipsum Nisi enim Reprehenderit",
    image:"http://fpoimg.com/400x400?text=",
    url:"#",
    id:"00000",
}, data); %>

<li class="block-event" data-id="<%=data.id%>" data-date="<%=data.date%>">
    <div class="content">
        <div class="date">
            <div class="month"><%=_.moment(data.date).format('MMM')%></div>
            <div class="day"><%=_.moment(data.date).format('D')%></div>
        </div>
        <a class="title" href="<%=data.url%>"><%=data.title%></a>
        <div class="time"><%=_.moment(data.date).format('h:mma')%></div>
    </div>
</li>

