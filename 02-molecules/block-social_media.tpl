<% data = _.extend({
    date:"2015-10-11 09:00",
    title:"Lorem Ipsum Nisi enim Reprehenderit",
    image:"",
    classes:'',
    type:'facebook',
    text:"A heartfelt thank you to LeVar Burton who inspired our graduates at Commencement. Students and guests alike were thrilled to sing Reading Rainbow theme song and be in the presence of Geordi La Forge.",

}, data); 


if(typeof data.type != "undefined" && data.type != ""){ 
    data.classes += ' type-'+data.type;
}

if(typeof data.image != "undefined" && data.image != ""){ 
    data.classes += ' has-image';
}

%>


<div class="block-social-media <%=data.classes%>">

    <% if(typeof data.image != "undefined" && data.image != ""){ %>
        <div class="image" style="background-image:url(<%=data.image%>)"></div>
    <% }%>

    <div class="text-content"><div class="text-content-inner">
        <div class="text"><%=data.text%></div>
        <a href="<%=data.link%>" class="action"><%=data.action%></a>
    </div></div>  
    
    <div class="user">

        <div class="icon"></div> 

        <a href="<%=data.link%>" class="screen-name"><%=data.username%></a>

        <span class="time"><%=data.postDateRelative%></span>

    </div>

</div>
 