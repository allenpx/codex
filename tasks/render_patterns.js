var fs = require('fs');
var _ = require('underscore');

var path = require('path');
var util = require('util');
var hl = require("highlight.js");
var moment = require('moment');
_.moment = moment;
hl.configure({
   tabReplace:"----" 
});

module.exports = function (grunt) {

    grunt.registerMultiTask('render_patterns', 'Render Patterns to html', function() {


        //make grunt know this task is async.
        var done = this.async();

        // Read the file and print its contents.
        var self = this;
        var data = this.data;

        var navigation = {
            children:{}
        };

        var filesArray = grunt.file.expand({}, data.files);
        var patterns = [];
        var basePattern = null;

        var codepageTemplateA = '<!DOCTYPE html><html><head><link href="http://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet" type="text/css">';
        codepageTemplateA += '<link rel="stylesheet" type="text/css" href="sourcecode.css"/>';
        codepageTemplateA += '<style>.hljs,pre,body{font-family:"Source Code Pro";}</style>';
        codepageTemplateA += '</head><body class="hljs"><pre>';
        var codepageTemplateB = '</pre></body></html>';

        fs.readdirSync(data.dest).forEach(function(fileName) {
            if (path.extname(fileName) === ".html") {
                fs.unlinkSync(path.normalize(data.dest+'/'+fileName));
            }
        });

        //First, compile all patterns and create navigation
        filesArray.forEach(function(filePath){

            var pattern = {};
            pattern.section = path.dirname(filePath).split('/').pop().split('-')[1];
            pattern.file = grunt.file.read(filePath);
            pattern.fileName = path.basename(filePath, ".tpl");
            pattern.subsection = pattern.fileName.split('-')[0];
            pattern.name = pattern.fileName.split('-').pop();
            pattern.fullname = pattern.section+'-'+pattern.fileName;
            pattern.template = _.template(pattern.file, null, {variable: 'data'});
            pattern.data = {};

            //load pattern data
            if(fs.existsSync(filePath.replace('.tpl', '.json'))){
                var localData = grunt.file.readJSON(filePath.replace('.tpl', '.json'));
                if(localData.variants != 'undefined'){
                    pattern.variants = localData.variants;
                }else{
                    pattern.data = grunt.file.readJSON(filePath.replace('.tpl', '.json'))
                }
            }

            //compile pattern intro a partial
            _.registerPartial(pattern.fullname, pattern.file);

            if(navigation.children[pattern.section] == undefined){
                navigation.children[pattern.section] = {
                    title:pattern.section.toTitleCase(),
                    children:{}
                };
            }
            if(navigation.children[pattern.section].children[pattern.subsection] == undefined){
                navigation.children[pattern.section].children[pattern.subsection] = {
                    title:pattern.subsection.replace(/\_/g, " ").toTitleCase(),
                    children:{}
                };
            }

            var navItem = {
                title:pattern.name.replace(/\_/g, " ").toTitleCase(),
                url:'#'+pattern.section+'/'+pattern.subsection+'/'+pattern.name
            }

            navigation.children[pattern.section].children[pattern.subsection].children[pattern.name] = navItem;

            if(pattern.fullname == "templates-base-base"){
                basePattern = pattern;
            }
            patterns.push(pattern);

        },this);

        //render each partial to html and save
        patterns.forEach(function(pattern){
            if(pattern.section != 'templates' && pattern.section != 'pages'){//we need to wrap the pattern in the base template
                
                var renderedPattern = "";
                var renderedPatternFragment = '';
                if(pattern.variants != undefined){
                    _.each(pattern.variants, function(variant){
                        variant.data = variant.data || {};
                        variant.rendered = pattern.template(variant.data);
                        renderedPatternFragment += variant.rendered +'\n\n';
                    });
                    renderedPattern = basePattern.template({
                        variants:pattern.variants
                    });
                }else{
                    renderedPatternFragment = pattern.template(pattern.data);
                    renderedPattern = basePattern.template({
                        body:renderedPatternFragment
                    });
                }


                grunt.file.write(data.dest+'/'+pattern.fullname+'.html', renderedPattern);
                grunt.file.write(data.dest+'/'+pattern.fullname+'.code.html', codepageTemplateA+hl.highlight('html',renderedPatternFragment).value+codepageTemplateB);

            }else{
                grunt.file.write(data.dest+'/'+pattern.fullname+'.html', pattern.template(pattern.data));
                grunt.file.write(data.dest+'/'+pattern.fullname+'.code.html', codepageTemplateA+hl.highlight('html',pattern.template(pattern.data)).value+codepageTemplateB);
            }

        },this);
        
        //console.log(JSON.stringify(navigation, null,4))
        //console.log('final:',util.inspect(navigation, {depth:10}))

        grunt.file.write(data.dest+'/navigation.js', 'window.PatternsNavigation = '+JSON.stringify(navigation, null,4)+';' );

        done();

    });



};


/**
 * Allow underscore use of partials
 */
var underscorePartials = (function(){
    var partialCache = {};
 
    var mixin = {
        registerPartial: function(name, template) {
            partialCache[name] = _.template(template, null, {variable: 'data'});
        },
        partial: function(name, data) {
            return partialCache[name](data)
        }
    };
 
    return mixin;
 
})();
 
_.mixin(underscorePartials)






String.prototype.toTitleCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};