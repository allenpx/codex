var fs = require('fs');
var _ = require('underscore');


module.exports = function (grunt) {

    grunt.registerMultiTask('compile_modules', 'Compile Less Modules', function() {
       // var po = require('node-po');
        var path = require('path');

        //make grunt know this task is async.
        var done = this.async();

        // Read the file and print its contents.
        var self = this;
        var data = this.data;

       // console.log(data)
        fs.readFile(data.src, 'utf8', function(err, fileData) {

            if (err) throw err;

            //console.log('OK: ' + data.src);
            //console.log(data);

            var moduleFunctions = extractFunctions(fileData);
            //console.log(moduleFunctions);

            fs.writeFile(data.dest, buildFile(moduleFunctions), function(){
                done();
            });

        });


        function extractFunctions(string){
            var arr = string.match(data.regex);
            return _.uniq(arr);
        }

        function buildFile(functionsArray){
            var stringFile = "";
            stringFile += '@import "modules.less";\n';
            _.each(data.breakpoints, function(breakpoint){

                if(breakpoint != 0){
                    stringFile += '\n.'+data.prefix+'breakpoint-'+breakpoint+'(){\n';
                }

                stringFile += '\t@bp:'+breakpoint+';\n';
                _.each(functionsArray, function(functionName){
                    stringFile += '\t'+functionName+data.functionOverloads+');\n';
                });

                if(breakpoint != 0){
                    stringFile += '}\n\n';
                }

            });

            return stringFile;

        }

    });



};