var fs = require('fs');
var _ = require('underscore');
var path = require('path');


module.exports = function (grunt) {

    grunt.registerMultiTask('colorize_svg', 'Colorize SVG', function() {
       // var po = require('node-po');
        var path = require('path');

        //make grunt know this task is async.
        var done = this.async();

        // Read the file and print its contents.
        var self = this;
        var data = this.data;

        var iconCSS = "";
        var colorFormatRegex = /\$name/g;
        var variations = [];


        var filesArray = grunt.file.expand({}, data.files);
        
        filesArray.forEach(function(filePath){

            var file = grunt.file.read(filePath);
            var fileName = path.basename(filePath, ".svg");

            var colorRegex = new RegExp("(?!fill=\")"+data.replace);
            iconCSS += data.cssFormat.replace(colorFormatRegex, fileName);

            data.colors.forEach(function(color){
                
                var colorized = file.replace(colorRegex,color.hex);
                
                grunt.file.write(data.dest+'/'+fileName+"-"+color.name+'.svg', colorized);

            },this);


        },this);

        grunt.file.write(data.cssDest, iconCSS);

        done();

    });



};