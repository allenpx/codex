<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10 no-flexbox" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <title></title>
    
    <meta name="viewport" content="minimal-ui, width=device-width,initial-scale=1, user-scalable=no">

    <link href="/system/template/v5/css/template.base.dev.css" rel="stylesheet" type="text/css" />
    <link href="/system/template/v5/css/template.responsive.dev.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/804448/791924/css/fonts.css" />

    <script type="text/javascript">window.afterLoad = [];</script>

</head>

<body class="layout-landing"><form name="form" runat="server" id="form" encType="multipart/form-data">



    <div class="page-container">

        <div class="menu">

            <div class="menu-overlay"></div>

            <div class="menu-button">
                <span class="menu-icon"><div class="a"></div><div class="b"></div><div class="c"></div></span><span>Menu</span>
            </div>

            <a href="#"  class="logo"><img src="/system/template/v5/images/logo.svg"></a>

            <nav class="primary" >
                    <div class="close-button">
                        <i class="icon icon-close white"></i>
                    </div>
                    <div class="scrollable root"><div>
                        <div class="forward-button"><i class="icon icon-arrow-right primary"></i><span></span></div>
                        <h2><a href="#">UMass Lowell</a></h2>
                        <ul>
                            <li><a href="#">Lowell Experience</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Academics</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Admissions &amp; Aid</a></li>
                            <li><a href="#">Student Life</a></li>
                            <li><a href="#">Athletics</a></li>
                        </ul>
                    </div></div>

                    <div class="scrollable current" ><div>

                            <a href="#" class="logo"><img src="/system/template/v5/images/logo.svg"></a>
                            <div class="back-button"><i class="icon icon-arrow-left primary"></i><span></span></div>
                            <h2><a href="#">Website Title</a></h2>
                            <ul>
                                <li><a href="#">Lorem ipsum dolor</a></li>
                                <li><a href="#">Sit amet consectetuer</a></li>
                                <li><a href="#">Elit sed</a></li>
                                <li><a href="#">Diam nonummy nibh</a></li>
                                <li><a href="#">Tincidunt ut</a></li>
                                <li><a href="#">Laoreet dolore magna</a></li>
                                <li><a href="#">Erat volutpat</a></li>
                            </ul>

                    </div></div>
                    
                
            </nav>

        </div>

    
        <div class="page">

            <%= _.partial('organisms-global-tools', {}) %>

            <%= _.partial('organisms-global-header', {
                title:"Wellness Wall",
                banner:"/system/template/v5/images/photos/Civil Engineering 20120424-23.jpg"
            }) %>

            <div class="components">


                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "sections":[
                        {
                            "text":"<p>Lorem ipsum Adipisicing do ut tempor in occaecat fugiat in quis elit irure ad aliquip pariatur dolore anim cillum aliquip veniam sed exercitation consequat et eiusmod fugiat ullamco in ut ad aliquip minim quis tempor.</p>"
                        },
                    ]
                }) %>


                <div class="component content">

                    <div class="grid">
                        <div class="cell small-1 medium-3"><div class="demo"></div></div>
                        <div class="cell small-2 medium-3"><div class="demo"></div></div>
                    </div>
                    <div class="grid strict">
                        <div class="cell small-3"><div class="demo"></div></div>
                        <div class="cell small-1"><div class="demo"></div></div>
                        <div class="cell small-2"><div class="demo"></div></div>
                    </div>

                </div>


                <div class="scrolling-sections-links-container component link-list image-tiles show-for-large-up"></div>


                <%= _.partial('organisms-content-events', {
                    title:"Events"
                }) %>

                

                <% 

                var test = "<h2>Occupational</h2><h3>Lorem ipsum Dolore elit sed elit</h3><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p><ul><li><a href='#'>Lorem ipsum Dolore</a></li><li><a href='#'>Lorem ipsum Dolore</a></li></ul>";
                test += '<div class="grid">';
                    test += '<div class="cell medium-6">';
                    test += "<ul><li><a href='#'>Lorem ipsum Dolore</a></li><li><a href='#'>Lorem ipsum Dolore</a></li><li><a href='#'>Lorem ipsum Dolore</a></li></ul>";
                    test += '</div>';
                    test += '<div class="cell medium-6">';
                    test += "<ul><li><a href='#'>Lorem ipsum Dolore</a></li><li><a href='#'>Lorem ipsum Dolore</a></li><li><a href='#'>Lorem ipsum Dolore</a></li></ul>";
                    test += '</div>';
                test += '</div>';

 
                %>

                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "classes":"scrolling-sections", 
                    "sections":[
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block.png",
                                "style":"center"
                            },
                            "heading":"Section-1",
                            "text":test
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block2.png",
                                "style":"center"
                            },
                            "heading":"Section-2",
                            "text":'<div class=text><span><h2>Physical</h2><h3>Taking Care of Your Body</h3><ul><li><a href=/campusrecreation>CRC &amp; RFC</a></li><li><a href=/personaltraining>Personal Training</a></li><li><a href=/gfit>Group Fitness</a></li><li><a href=/oap>Outdoor Adventure</a></li><li><a href=/intramurals>Intramural Sports</a></li><li><a href=/freewheelers>FreeWheelers</a></li><li><a href=/bikeshop>UML Bike shop</a></li><li><a href=/fitness>Fitness Programs</a></li><li><a href=/CampusRecreation/Fitness-Wellness/Health2U.aspx>Health2U</a></li><li><a href=/CampusRecreation/Fitness-Wellness/Wellness-Programs/Commit-To-Be-Fit.aspx>Commit-To-Be Fit Program</a></li><li><a href=/CampusRecreation/Fitness-Wellness/Wellness-Programs/HealthyHabit.aspx>Healthy Habit Program</a></li><li><a href="http://umasslowell.campusdish.com/">University Dining</a></li><li><a href="/student-services/health/">Health Services/Health Education</a></li><li><a href="/student-services/health/">Flu clinics &amp; info</a></li><li><a href=/Health-Sciences/default.aspx>College of Health Sciences</a></li><li><a href=/stressreliefday>Stress Relief Day</a></li><li><a href=https://www.facebook.com/UmlHealthyHawks target=_blank>Healthy Hawks -</a></li><li>Wellness Wednesdays (no links yet)</li><li><a href=/HR/Healthy-Workplace.aspx>Healthy Workplace (fac/staff)</a></li><li><a href="http://umasslowell.campusdish.com/">University Dining</a></li><li><a href=/Innovation-Entrepreneurship/DifferenceMaker/Meet-the-DifferenceMakers/DM-SupportOurStudents.aspx>Support Our Students (deals with hunger issues)</a></li><li><a href=/student-services/Health/STDs.aspx>STD\'s Info</a></li><li><a href="/student-services/Veterans/">Veteran\'s Services</a></li></ul><h4>Alcohol &amp; Drug Programs/Resources</h4><ul><li><a href=/student-services/Counseling/Additional-Resources/Drugs-Alcohol.aspx>Counseling</a></li><li><a href="https://interwork.sdsu.edu/echeckup/usa/alc/coll/index.php?id=UML&amp;hfs=true" target=_blank>E-checkup to go alcohol assessment</a></li><li><a href=/student-services/Health/Health-Education-Resources/Alcohol-and-Drug-Information.aspx>Health Education</a></li><li><a href=/Police/Community-Outreach/RAD-System.aspx>RAD (Rape Aggression Defense):</a></li><li><a href=/police>Campus Police (Personal Safety):</a></li><li>Smoking Cessation</li></ul></span></div>'
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block.png",
                                "style":"center"
                            },
                            "heading":"Section-3",
                            "text":"<h2>Occupational</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block2.png",
                                "style":"center"
                            },
                            "heading":"Section-4",
                            "text":"<h2>Social</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block.png",
                                "style":"center"
                            },
                            "heading":"Section-5",
                            "text":"<h2>Occupational</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block2.png",
                                "style":"center"
                            },
                            "heading":"Section-6",
                            "text":"<h2>Social</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block.png",
                                "style":"center"
                            },
                            "heading":"Section-7",
                            "text":"<h2>Occupational</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block2.png",
                                "style":"center"
                            },
                            "heading":"Section-8",
                            "text":"<h2>Social</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/color-block.png",
                                "style":"center"
                            },
                            "heading":"Section-9",
                            "text":"<h2>Occupational</h2><p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },

                    ]
                }) %>

                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "sections":[
                        {
                            "text":"<p>Lorem ipsum Voluptate magna nisi eu Ut sed veniam Ut aliqua ad Duis sed fugiat ullamco quis dolore ex id sunt adipisicing enim esse sed eu adipisicing velit cillum officia ut in reprehenderit enim quis nisi enim occaecat aute nisi dolore adipisicing adipisicing consequat eu consequat proident magna velit do anim nostrud fugiat dolore do nostrud dolore in culpa incididunt do tempor magna Ut enim velit et nulla consectetur aute veniam dolore cillum nostrud amet nisi eu consectetur dolor laborum laboris labore cupidatat commodo proident Ut consectetur reprehenderit amet dolore esse labore dolore Duis commodo minim sit quis enim ea proident cupidatat labore elit magna non nulla veniam adipisicing dolor aute aliquip irure tempor ut ex in sit ea in ullamco labore Ut ex ut dolor ut anim in ut pariatur occaecat amet ad non dolor fugiat pariatur veniam enim Duis nisi dolor Duis occaecat cupidatat laborum irure do eu nulla proident veniam dolore fugiat ea aliqua nulla commodo est qui ut sint ad Excepteur quis eu veniam consectetur irure in sit mollit incididunt anim magna anim adipisicing sit sunt enim irure veniam commodo sunt deserunt Ut sed cillum est proident incididunt qui exercitation ad tempor.</p>"
                        },
                    ]
                }) %>
                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "sections":[
                        {
                            "text":"<p>Lorem ipsum Adipisicing do ut tempor in occaecat fugiat in quis elit irure ad aliquip pariatur dolore anim cillum aliquip veniam sed exercitation consequat et eiusmod fugiat ullamco in ut ad aliquip minim quis tempor.</p>"
                        },
                    ]
                }) %>

                    
            </div>







            <div class="margin-clear"></div>
        </div>
    </div>
    <div class="float-clear"></div>

    <%= _.partial('organisms-global-footer', {}) %>

    <script src="/system/template/v5/js/template.dev.js" type="text/javascript"></script>

    <!-- ### Remove in Production 
    ###-->
    <script type="text/javascript" src="http://129.63.83.158:6542/livereload.js"></script>
    <!-- ### Remove in Production ###-->


</form></body>

</html>











