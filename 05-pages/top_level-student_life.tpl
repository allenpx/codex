<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 no-flexbox" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10 no-flexbox" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <title></title>
    
    <meta name="viewport" content="minimal-ui, width=device-width,initial-scale=1, user-scalable=no">

    <link href="/system/template/v5/css/template.base.dev.css" rel="stylesheet" type="text/css" />
    <link href="/system/template/v5/css/template.responsive.dev.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/804448/791924/css/fonts.css" />

    <script type="text/javascript">window.afterLoad = [];</script>

</head>

<body class=""><form name="form" runat="server" id="form" encType="multipart/form-data">


    <%= _.partial('organisms-global-header', {
        //classes:'dark-primary-nav'
    }) %>

    <%= _.partial('organisms-global-marquee', {
        title:"Student Life",
        banner:"/system/template/v5/images/photos/Gazebo Library-42.jpg"
    }) %>
 



    <div class="layout page-container" style="">

        <div class="sidebar">

            <nav class="secondary">

                <h2><span><a href="#">Website Title</a></span></h2>
                <ul>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Sit amet consectetuer</a></li>
                    <li><a href="#">Elit sed</a></li>
                    <li><a href="#">Diam nonummy nibh</a></li>
                    <li><a href="#">Tincidunt ut</a></li>
                    <li><a href="#">Laoreet dolore magna</a></li>
                    <li><a href="#">Erat volutpat</a></li>
                </ul>
            </nav>

        </div>
        <div class="page">
            
            <div class="components">

                <%= _.partial('organisms-content-social_media', {

                }) %>


                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "classes":"featured", 
                    "sections":[
                        {
                            "media":{
                                "type":"video",
                                "url":"http://www.uml.edu/Brightcove/Video.aspx?video_id=3706509092001",
                                "style":"right"
                            },
                            "heading":"Lorem Ipsum Voluptate",
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },

                    ]
                }) %>


                <%= _.partial('organisms-content-main_content', {
                    "heading":"",
                    "classes":"featured", 
                    "sections":[
                        {
                            "media":{
                                "type":"image",
                                "url":"/system/template/v5/images/photos/Durgan Hall-69.jpg",
                                "style":"left"
                            },
                            "heading":"Lorem Ipsum Voluptate",
                            "text":"<p>Lorem ipsum Voluptate culpa culpa occaecat fugiat magna nostrud laborum dolor ullamco nostrud dolore sit exercitation eu in esse enim in sint et cupidatat adipisicing aliquip eiusmod officia.</p>"
                        },

                    ]
                }) %>


                <%= _.partial('organisms-content-news', {
                    title:""
                }) %>

                <%= _.partial('organisms-content-events', {
                    title:"Events"
                }) %>




            </div>

            <div class="margin-clear"></div>
        </div>
    </div>
    <div class="float-clear"></div>

    <%= _.partial('organisms-global-footer', {}) %>

    <script src="/system/template/v5/js/template.dev.js" type="text/javascript"></script>

    <!-- ### Remove in Production 
    ###-->
    <script type="text/javascript" src="http://129.63.83.158:6542/livereload.js"></script>
    <!-- ### Remove in Production ###-->


</form></body>

</html>











