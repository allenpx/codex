var component = require('lib/ui/component');

var Handle = component.extend(/** @lends ui.Handle.prototype */{

	initialize:function(options){
        
        this._super(options);

        this.$el.on('mousedown', $.proxy(this.mousedown, this));
        this.$el.on('dblclick', $.proxy(function(event){
        	this.emit("dblclick", event);
        }, this));
       
        
		
	},

	mouseX:0,
	mouseY:0,
	isDown:false,
	mousedown:function(event){
		this.mouseX = event.pageX;
		this.mouseY = event.pageY;
		this.emit("mousedown");
		$body.on('mousemove.viewerhandle', $.proxy(this.mousemove, this));
		$body.on('mouseup.viewerhandle', $.proxy(this.mouseup, this));
	},
	mouseup:function(event){
		this.emit("mouseup");
		$body.off('mousemove.viewerhandle');
		$body.off('mouseup.viewerhandle');
	},
	mousemove:function(event){
		var mouseXDifference = this.mouseX-event.pageX;
		var mouseYDifference = this.mouseY-event.pageY;
		this.emit("update", {x:mouseXDifference, y:mouseYDifference});
	}


});

module.exports = Handle;





