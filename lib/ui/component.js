var Backbone = require('backbone');
var _ = require('underscore');
var Component = Backbone.View.extend({
	
	model:null,
	defaultOptions:{
		
	},
 	initialize:function(options){

 		this._super(options);

 		if(options == undefined){
 			options = {};
 		}

 		this.options = _.extend({}, options || {}, this.defaultOptions,{
 			initDefaultModel:true
 		});


 		if(options.classes){
 			this.options.classes += ' '+options.classes; 
 		}

 		if(this.options.initDefaultModel){
 			this.model = new Backbone.Model();

 		}
 		
 		this.$el.addClass(this.options.classes);

 		this.emit = this.trigger;

 	},

 	render:function(){

 		return this;
 	},
 	update:function(){

 	},
});

module.exports = Component;
