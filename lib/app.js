
var Backbone = require('backbone');
require('backbone-super');

var menu = require('lib/core/menu');
var layout = require('lib/core/layout');
var router = require('lib/core/router');
require('mousetrap');

var App = Backbone.View.extend({

	initialize:function(attributes, options){

		this._super(attributes, options);

        this.menu = new menu();
        this.layout = new layout();
        window.Menu = this.menu;
        window.Layout = this.layout;

        this.router = new router();
        this.router.on('route:view', this.setView, this);

        Backbone.history.start();

        Mousetrap.bind('ctrl+1', $.proxy(this.toggleFullscreen, this)); 
		 
	},

	router:null,

	setView:function(section, subsection, name){
		this.layout.setContent('rendered/'+section+'-'+subsection+'-'+name+'.html'+"?decache="+new Date().getTime() );

        var path = window.location.host+window.location.pathname+'/rendered/'+section+'-'+subsection+'-'+name+'.html';
        path = path.replace('index.html', '');
        path = path.replace('//', '/');

		$('#open-page').attr("href", window.location.protocol+"//"+path);
	},
	isFullscreen:false,
	toggleFullscreen:function(){
		if(this.isFullscreen){
			this.exitFullscreen();
		}else{
			this.enterFullscreen();
		}
	},
	enterFullscreen:function(){
		$body.addClass("fullscreen");
		this.isFullscreen = true;
	},
	exitFullscreen:function(){
		$body.removeClass("fullscreen");
		this.isFullscreen = false;
	}

});

module.exports = App;
