var app = require('lib/app');
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
Backbone._ = _;

global._ = _;
global.$ = $;
global.jQuery = global.$;
global.$window = null;
global.$html = null;
global.$body = null;
global.$root = $({});
domain = "";

/**
 * @module Main
 * @requires core.App
 */
$(document).ready(function(){

	$window = $(window);
	$html = $('html');
	$body = $('body');
	$sectionApp = $('.section-app');

	if(window.navigator.standalone){
	    $body.addClass("ios7-standalone");
	}

	_.defer(function(){
		$body.addClass("anim");
	})

	window.app = new app();

});


String.prototype.toTitleCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};