var Backbone = require('backbone');
var _ = require('underscore');

var Menu = Backbone.View.extend({


	initialize:function(){

        this.$menu = $('header.toolbar nav.menu');
        this.build();

        $('header.toolbar nav>ul>li>span').on('click', $.proxy(this.menuClick, this));
        $('header.toolbar nav a').on('click', $.proxy(function(event){
			$('header.toolbar nav>ul>li.open').removeClass('open');
			$body.removeClass('ignore-iframes');
        }, this));
		
	},

	$menu:null,

	menuClick:function(event){
		if($(event.currentTarget).parent().hasClass('open')){
			$(event.currentTarget).parent().removeClass('open');
			$body.removeClass('ignore-iframes');
		}else{
			$('header.toolbar nav>ul>li.open').removeClass('open');
			$(event.currentTarget).parent().addClass('open');
			$body.addClass('ignore-iframes');

			$body.on('mousedown.menu', $.proxy(function(event){
			    var container = $('header.toolbar nav');

			    if (!container.is(event.target) && container.has(event.target).length === 0){
			        $('header.toolbar nav>ul>li.open').removeClass('open');
			        $body.off('mousedown.menu');
			        $body.removeClass('ignore-iframes');
			    }
			}, this) );

		}

	},

	build:function(){
		this.$menu.empty();
		this.$menu.append(this.buildMenuFragment(PatternsNavigation.children, $('<ul/>')));
	},

	buildMenuFragment:function(childrenArray, $parentElement){

		_.each(childrenArray, function(child){

			if(child.url != undefined){
				var $item = $('<li><a href="'+child.url+'">'+child.title+'</a></li>');
			}else{
				var $item = $('<li><span>'+child.title+'</span></li>');
			}

			if(child.children != undefined){ 
				if(_.keys(child.children).length){
					$item.addClass('has-children');
					$item.append(this.buildMenuFragment(child.children, $('<ul/>')));
				}
			}

			$parentElement.append($item);

		},this);

		return $parentElement;

	}


});

module.exports = Menu;







