var Backbone = require('backbone');
var _ = require('underscore');
var handle = require('lib/ui/handle');
require('lib/plugins/html5storage.js');


var Layout =  Backbone.View.extend(/** @lends core.Layout.prototype */{

	initialize:function(options){

        this.$viewContent = $('.viewer');
        this.$viewContentCenter = $('.viewer .resizer');
        this.$sourceContent = $('.source');

        if($.sessionStorage.getItem('layout_source_height') == null){
        	$.sessionStorage.setItem('layout_source_height',25);
        	$.sessionStorage.setItem('layout_source_open',false);
        }

        this.sourceOpen = $.sessionStorage.getItem('layout_source_open',"boolean");

        this.viewHandle = new handle({el:$('.viewer .handle')});
        this.viewHandle.on('mousedown', this.disableContentWindow, this);
        this.viewHandle.on('mouseup', this.enableContentWindow, this);
        this.viewHandle.on('update', this.updateViewerSize, this);

        this.sourceHandle = new handle({el:$('.source .handle')});
        this.sourceHandle.on('mousedown', this.disableContentWindow, this);
        this.sourceHandle.on('mouseup', this.enableContentWindow, this);
        this.sourceHandle.on('dblclick', this.toggleSource, this);
        this.sourceHandle.on('update', this.updateSourceSize, this);
        
        this.$viewContentCenter.css('width', $.sessionStorage.getItem('layout_view_width',"float"));

        if(this.sourceOpen == true){
        	this.$sourceContent.css('height', $.sessionStorage.getItem('layout_source_height', "float")+'%');
        	this.$viewContent.css('height',  (100-$.sessionStorage.getItem('layout_source_height', "float")) +'%');
        }else{
        	this.$sourceContent.css('height', '0%');
        	this.$viewContent.css('height', '100%');
        }
		
		 
	},

	handle: null,
	viewerWidth:0,
	viewerHeight:0,
	sourceHeight:0,
	$viewContent:null,
	$sourceContent:null,

	updateViewerSize:function(event){
		var newWidth = this.viewerWidth - (event.x*2);
		this.$viewContentCenter.css('width', newWidth);
		$.sessionStorage.setItem('layout_view_width', newWidth);
	},
	updateSourceSize:function(event){
		var newHeight = this.sourceHeight + (event.y);

		var newHeightPercentage = (newHeight/($body.height()))*100;

		this.$sourceContent.css('height', newHeightPercentage+'%');
		if(this.$sourceContent.height() < 50){
			this.hideSource();
		}
		this.$viewContent.css('height', (100-newHeightPercentage)+'%' );
		$.sessionStorage.setItem('layout_source_height', newHeightPercentage);
	},
	disableContentWindow:function(){
		this.viewerWidth = this.$viewContentCenter.width();
		this.sourceHeight = this.$sourceContent.height();
		$('.layout').addClass('resizing');
		$body.addClass('ignore-iframes');
	},
	enableContentWindow:function(){
		$('.layout').removeClass('resizing');
		$body.removeClass('ignore-iframes');
	},

	setContent:function(url){

		$('.viewer iframe')[0].src = url;
		$('.source iframe')[0].src = url.replace('.html', '.code.html');
		
	},

	sourceOpen:false,
	toggleSource:function(){
		if(this.sourceOpen == true){
			this.hideSource();
		}else{
			this.showSource();
		}
		$.sessionStorage.setItem('layout_source_open', this.sourceOpen);
	},
	hideSource:function(){
		console.log('hideSource')
		this.sourceOpen = false;
		if(this.$sourceContent.height() < 50){
			$.sessionStorage.setItem('layout_source_height',25);
		}else{
			$.sessionStorage.setItem('layout_source_height', this.$sourceContent[0].style.height );
		}
		this.$viewContent.addClass('animated');
		this.$sourceContent.addClass('animated');
		this.$sourceContent.css('height', '0%');
		this.$viewContent.css('height', '100%');

		_.delay($.proxy(function(){
			this.$viewContent.removeClass('animated');
			this.$sourceContent.removeClass('animated');
		},this),250 );
	},
	showSource:function(){
		console.log('showsource')
		this.sourceOpen = true;

		if( ($.sessionStorage.getItem('layout_source_height',"float")/$body.height() ) < 50 ){
			$.sessionStorage.setItem('layout_source_height',25);
		}

		this.$viewContent.addClass('animated');
		this.$sourceContent.addClass('animated');
		this.$sourceContent.css('height', $.sessionStorage.getItem('layout_source_height')+'%');
		this.$viewContent.css('height', (100 - $.sessionStorage.getItem('layout_source_height')) + '%');

		_.delay($.proxy(function(){
			this.$viewContent.removeClass('animated');
			this.$sourceContent.removeClass('animated');
		},this),250 );
	},


	setSize:function(size){
		this.$viewContent.addClass('animated');
		this.$viewContentCenter.css('width', size);
		_.delay($.proxy(function(){
			this.$viewContent.removeClass('animated');
		},this),250 );


		$.sessionStorage.setItem('layout_view_width', size);
	}


});

module.exports = Layout;



