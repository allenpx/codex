
var pkg = require('./package');

module.exports = function(grunt) {

    var liveReloadPort = 5427;

    grunt.initConfig({

        pkg: pkg,

        icons: {
            build: {
                icons:['node_modules/pristine-icons/source/*.svg'],
                namespace:'icon',
                lessDest:'less/config/icons.less',
                iconsDest:'images/icons',
                variants:[
                    {
                        'name':'color',
                        'primary':"#0469B1",
                        'secondary':"#67B6BD"
                    },
                    {
                        'name':'white',
                        'primary':"#ffffff",
                        'secondary':"#ffffff"
                    },
                    {
                        'name':'black',
                        'primary':"#black",
                        'secondary':"#black"
                    },
                    {
                        'name':'primary',
                        'primary':"#0469B1",
                        'secondary':"#0469B1"
                    },
                    {
                        'name':'secondary',
                        'primary':"#67B6BD",
                        'secondary':"#67B6BD"
                    },
                    {
                        'name':'tertiary',
                        'primary':"#FAC537",
                        'secondary':"#FAC537"
                    }
                ]
            }
        },

        svgmin: {
            options: {
                plugins: [
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            icons: {
                files: [{
                    expand:true,
                    cwd: 'images/icons',
                    dest: 'images/icons',
                    src: ['*.svg'],
                    ext: '.svg'
                }]
            }
        },

        uglify: {
            js:{
                options: {
                    compress:true,
                    mangle:false,
                    sourceMap:false
                },
                files: {
                    'js/template.js': ['js/template.dev.js']
                }
            },
        },

        less: {
            dev: {
                options: {
                    paths: [
                        "less",
                        "node_modules",
                    ],
                    compress: true,
                    sourceMap:true,
                    strictImports:true,
                },
                files: {
                    "css/template.base.dev.css":  "less/build/template.base.less",
                    "css/template.responsive.dev.css":  "less/build/template.responsive.less",
                }
            },
            prd: {
                options: {
                    paths: [
                        "less",
                        "node_modules",
                    ],
                    compress: true,
                    sourceMap:false,
                    strictImports:true,
                },
                files: {
                    "css/template.base.css":  "less/build/template.base.less",
                    "css/template.responsive.css":  "less/build/template.responsive.less",
                }
            }

        },

        'template-module': {
            templates:{
                files: "lib/tpl/**/*.tpl",
                options: {
                    module: true,
                    single:true,
                    provider:'handlebars',
                    templateSettings:{
                        evaluate:    /\{\{(.+?)\}\}/g,
                        interpolate: /\{\{=(.+?)\}\}/g,
                        escape:      /\{\{-(.+?)\}\}/g
                    }
                }
            }
        },

        filelist:{
            getAll_Template_LessModules:{
                src: [
                    'less/modules/template/**/*.less',
                ],
                dest: 'less/config/template.modules.less',
                transform:'@import "{{filepath}}";'
            },
            getAll_Search_LessModules:{
                src: [
                    'less/modules/search/**/*.less',
                ],
                dest: 'less/config/search.modules.less',
                transform:'@import "{{filepath}}";'
            },
        },

        browserify: {

            template: {
                src: ['lib/main.js'],
                dest: 'js/template.dev.js',
                options: {
                    debug:true,
                    alias: [
                      'jquery',
                      'backbone',
                      'underscore',
                      'class-inheritance',
                      'lib/plugins/base',
                      'lib/page'
                    ],
                },
            },

        },

        render_patterns: {
            build: {
                files:['01-atoms/**/*.tpl','02-molecules/**/*.tpl','03-organisms/**/*.tpl','04-templates/**/*.tpl','05-pages/**/*.tpl'],
                dest: 'rendered',
            }
        },

        watch:{
            html: {
                files: ['*.html'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            less: { 
                files: [
                    'less/**/*.less',
                    '!less/config/modules.less',
                    'node_modules/ui/less/**/*.less',
                ],
                tasks: ['filelist','less'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            templates: { 
                files: ['lib/tpl/**/*.tpl'],
                tasks: ['template-module','browserify', 'uglify'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            patterns: {
                files: ['01-atoms/**/*','02-molecules/**/*','03-organisms/**/*','04-templates/**/*','05-pages/**/*'],
                tasks: ['render_patterns','browserify', 'uglify'],
                options: {
                  spawn: true,
                  interrupt: true,
                  livereload:{
                        port:liveReloadPort
                    }
                },

            },
            js: {
                files: [
                    'lib/**/*.js',
                    'lib/**/*.json',
                    'node_modules/ui/lib/**/*.js',
                ],
                tasks: [
                    'browserify', 
                    'uglify'
                ],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-tasks');
    grunt.loadNpmTasks('grunt-template-module');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadTasks('./tasks');

    grunt.registerTask('default', [
        'render_patterns',
        'icons',
        'svgmin',
        'template-module',
        'filelist',
        'browserify',
        'uglify',
        'less',
        'watch'
    ]);


};
